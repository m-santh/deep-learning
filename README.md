# Using Deep Learning to predict produce nutrients

![Title Screen](./screen1.png)

This project helps finding correlation via reflection scans and output of wet lab chemistry.

## Setup

Python is required for running the code in `main.py`.

The following packages need to be installed (prefarrably via pip3)

```
pip3 install --user numpy
pip3 install --user pandas
pip3 install --user keras
pip3 install --user tensorflow
pip3 install --user sklearn
pip3 install --user matplotlib
```

## Running the Project

Run the project with `python3 main.py`.

### Parametrize

Change parameters here.
Threshold is for the classification of produce and thus is the output of the prediction (bad - good). It needs to be adjusted when changing variety or target measurement (from poly to antioxidants for instance). When starting the program, a histogram pops up and shows distribution of target values to help deciding on a threshold.

![Histogram of Values](./screen2.png)

```python
scan_type = "produce.rawscan1"

input_keys = (
    "{}.data.median_365".format(scan_type),
    "{}.data.median_385".format(scan_type),
    "{}.data.median_450".format(scan_type),
    "{}.data.median_500".format(scan_type),
    "{}.data.median_530".format(scan_type),
    "{}.data.median_587".format(scan_type),
    "{}.data.median_632".format(scan_type),
    "{}.data.median_850".format(scan_type),
    "{}.data.median_880".format(scan_type),
    "{}.data.median_940".format(scan_type)
)

csv_file = "data_rfc_2019.csv"
variety_column = "sample_type"
variety = "cherry_tomato"
target_column = "produce.get_chemisty.data.polyphenols_value"

threshold = 125
```

### Network Architecture

The following lines define the Deep Learning architecture. Degrees of freedom are amounts of layers, activation function and amount of output parameters.
The output parameter of the last Layer needs to match the amount of classes for classification. In this case *good* (`1`) or *bad* (`0`).

```python
model = Sequential()

model.add(Dense(7, input_dim=len(input_keys), activation='sigmoid'))
model.add(Dense(5, activation='relu'))
model.add(Dense(2, activation='softmax'))
```

### Mapping of values to classes

The below code maps values to two classes (`1` or `0`). Instead of having a threashold the occurance of a string could be used for prediction (such as *organic*).

```python
for idx, v in enumerate(y_float):
    #y[idx] = [0 if ("organic" in v) else 1]
    #h[idx] = 0 if ("organic" in v) else 1
    y[idx] = [0 if v < threshold else 1]
    count += y[idx][0]
```